package com.graphcourse.graph;

/**
 * @param <N> - имя вершины
 * @param <T> - данные вершины
 */

public class Vertex <N, T> {
	
	private N name;
	private T data;
	private int index;
	
	public Vertex(){
		this.name = null;
		this.data = null;
		this.index = -1;
	}
	
	public Vertex(N name, T data){
		this.name = name;
		this.data = data;
		this.index = -1;
	}
	
	public Vertex(N name, T data, int index){
		this.name = name;
		this.data = data;
		this.index = index;
	}
	
	public boolean Compare(Vertex v){
		if(this.name == v.getName() && this.data == v.data)
			return true;
		return false;
	}

	public N getName() {
		return name;
	}

	public void setName(N name) {
		this.name = name;
	}

	public T getData() {
		return data;
	}

	public void setData(T data) {
		this.data = data;
	}

	public int getIndex() {
		return index;
	}

	public void setIndex(int index) {
		this.index = index;
	}
	
	@Override
	public String toString(){
		return index + " ";
	}
	
	public class VertexIterator {
		private Vertex myVertex = null;
		
		public VertexIterator(){
			
		}
	}
	
}
