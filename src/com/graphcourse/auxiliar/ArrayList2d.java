package com.graphcourse.auxiliar;

import java.util.ArrayList;

public class ArrayList2d<T>
{
	ArrayList<ArrayList<T>>	array;
 
	public ArrayList2d()
	{
		array = new ArrayList<ArrayList<T>>();
	}
 
	/**
	 * ensures a minimum capacity of num rows. Note that this does not guarantee
	 * that there are that many rows.
	 * 
	 * @param num
	 */
	public void ensureCapacity(int num)
	{
		array.ensureCapacity(num);
	}
 
	/**
	 * Ensures that the given row has at least the given capacity. Note that
	 * this method will also ensure that getNumRows() >= row
	 * 
	 * @param row
	 * @param num
	 */
	public void ensureCapacity(int row, int num)
	{
		ensureCapacity(row);
		while (row < getNumRows())
		{
			array.add(new ArrayList<T>());
		}
		array.get(row).ensureCapacity(num);
	}
 
	/**
	 * Adds an item at the end of the specified row. This will guarantee that at least row rows exist.
	 */
	public void Add(T data, int row)
	{
		ensureCapacity(row);
		while(row >= getNumRows())
		{
			array.add(new ArrayList<T>());
		}
		array.get(row).add(data);
	}
	
	public void Add(int row)
	{
		ensureCapacity(row);
		while(row >= getNumRows())
		{
			array.add(new ArrayList<T>());
		}
	}
	
	
	public void Add(T data, int row, int col)
	{
		ensureCapacity(row, col);
		set(row, col, data);
	}
	
	//By me
	public void PrintArray(){
		for(int i = 0; i < array.size(); i++){
			for(int j = 0; j < array.get(i).size(); j++){
				if(array.get(i).get(j) != null)
					System.out.print(array.get(i).get(j).toString() + "");
				else
					System.out.print("-1");
			}
			System.out.println("");
		}
	}
	
	public T get(int row, int col)
	{
		return array.get(row).get(col);
	}
 
	public void set(int row, int col, T data)
	{
		array.get(row).set(col,data);
	}
 
	public void remove(int row, int col)
	{
		array.get(row).remove(col);
	}
	
	public void delRow(int row){
		array.remove(row);
	}
	
	public void delCol(int col){
		for(int i = 0; i < array.size(); i++)
			array.get(i).remove(col);
	}
	
	/*
	public boolean contains(T data)
	{
		for (int i = 0; i < array.size(); i++)
		{
			if (array.get(i).contains(data))
			{
				return true;
			}
		}
		return false;
	}
 	*/
	public int getNumRows()
	{
		return array.size();
	}
 
	public int getNumCols(int row)
	{
		return array.get(row).size();
	}
}