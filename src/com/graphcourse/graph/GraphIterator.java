package com.graphcourse.graph;

public interface GraphIterator {
	public boolean begin();
	public boolean end();
	public boolean next();
	public Vertex get();
}
