package com.graphcourse.auxiliar;

import java.util.ArrayList;
import java.util.Random;

import com.graphcourse.graph.Edge;
import com.graphcourse.graph.Graph;
import com.graphcourse.graph.LGraph;
import com.graphcourse.graph.MGraph;
import com.graphcourse.graph.Vertex;
import com.graphcourse.gui.MainFrame;

/**
 * @param <VN> тип имени вершины
 * @param <VT> тип данных вершины
 * @param <EN> тип веса ребра
 * @param <ET> тип данных ребра
 */

public class GraphGenerator <VN, VT, EN, ET>{
	
	private int vNum, eNum;
	private boolean directType, graphType;
	private Random rand = new Random();
	private ArrayList <Vertex<Integer, Integer>> vertexArr = new ArrayList<>();
	private Graph graph;
	
	/**
	 * @param vNum - кол.во вершин
	 * @param eNum - кол.во ребер
	 * @param directType - flase = неорграф, true = орграф
	 * @param graphType - L-граф = false, M-граф = true
	 * @return
	 */
	public Graph GenerateGraph(int vNum, int eNum, boolean directType, boolean graphType){
		this.vNum = vNum;
		this.directType = directType;
		this.graphType = graphType;
		
		int eMuxNum = vNum * (vNum - 1);
		this.GenerateVertexArr(vNum);
		
		if (directType) { //орграф
			if (eNum > eMuxNum) {
				MainFrame.GetInstace().AddConsoleMsg("eNum > eMaxNum => eNum = eMaxNum");
				this.eNum = eMuxNum;
			}
		}
		else{ //неорграф
			if (eNum > eMuxNum / 2) {
				MainFrame.GetInstace().AddConsoleMsg("eNum > eMaxNum => eNum = eMaxNum ");
				this.eNum = eMuxNum / 2;
			}
		}
		
		//double p = 2.0 * eNum / vNum / (vNum - 1);
		
		if(graphType)
			graph = new MGraph(directType);
		else
			graph = new LGraph(directType);

		
		for(int i = 0; i < vNum; i++){
			graph.insertVertex(new Integer(rand.nextInt(15)), new Integer(rand.nextInt(15)) );
		}
		
		for(int i = 0; i < eNum; i++){
			int vInID = rand.nextInt(vNum);
			int vOutID = rand.nextInt(vNum);
			while(vInID == vOutID)
				vOutID = rand.nextInt(vNum);
			graph.insertEdge(vOutID, vInID, rand.nextInt(10), rand.nextInt(10));
		}

		return graph;
	}
	
	// Integer generator
	public void GenerateVertexArr(int vNum){
		for(int i = 0; i < vNum; i++){
			vertexArr.add(new Vertex<Integer, Integer>( rand.nextInt(), rand.nextInt(), i));
		}
	}
	
	public ArrayList<Vertex<Integer, Integer>> GetVertexArr(){
		return vertexArr;
	}
	
	public int GetvNum(){
		return vNum;
	}
	
	public int GeteNum(){
		return eNum;
	}
}