package com.graphcourse.graph;

import com.graphcourse.auxiliar.ArrayList2d;
import com.graphcourse.gui.MainFrame;

/**
 * M-граф, 
 * @param <N> - тип веса ребра и имя вершин
 * @param <T> - тип данных ребра и вершин
 */

public class MGraph<VN, VT, EW, ET> extends Graph<VN, VT, EW, ET>{
	
	private ArrayList2d<Edge> adjList;
	
	public MGraph(){
		this.directType = true;
		this.graphType = true;
		adjList = new ArrayList2d<Edge>();
		adjList.ensureCapacity(10);
		vNum = 0; 
		eNum = 0;
	}
	
	public MGraph(boolean directType){
		this.directType = directType;
		this.graphType = true;
		adjList = new ArrayList2d<Edge>();
		adjList.ensureCapacity(10);
		vNum = 0; 
		eNum = 0;
	}
	
	@Override
	public boolean insertEdge(int ivOut, int ivIn, EW weight, ET data) {
		
		if(ivOut > vNum || ivOut < 0 || ivIn > vNum || ivIn < 0)
			return false;
				
		if(eNum >= getEdgeMax())
			return false;
		
		Vertex vIn = vertexList.get(ivIn);
		Vertex vOut = vertexList.get(ivOut);
		
		if(vOut == null || vIn == null)
			return false;

		Edge e = new Edge<EW, ET>(vOut, vIn, weight, data);
		adjList.set(e.getvOut().getIndex(), e.getvIn().getIndex(), e);
		
		if(directType == false) //если неорграф
			adjList.set(e.getvIn().getIndex(), e.getvOut().getIndex(), new Edge(e.getvOut(), e.getvIn(), e.getWeight(), e.getData()));

		eNum++;
		return true;
	}

	@Override
	public boolean insertVertex(VN name, VT data) {
		
		Vertex <VN, VT> v = new Vertex <VN, VT>(name, data, vNum);
		this.vertexList.add(v);
		
		////// ДО ДОБАВЛЕНИЯ ИЛИ ПОСЛЕ???? ТЕСТЫ!!
		vNum++;
		
		for(int i = 0; i < vNum; i++){
			if(i < vNum - 1){
				adjList.Add(null, i);
			}
			else
				for(int j = 0; j < vNum; j++)
					adjList.Add(null, i);
		}
		return true;
	}

	@Override
	public boolean deleteEdge(int ivOut, int ivIn) {
		
		if(ivOut > vNum || ivOut < 0 || ivIn > vNum || ivIn < 0){
			return false;
		}
		
		Edge e = adjList.get(ivOut, ivIn);
		e = null;
		adjList.set(ivOut, ivIn, null);
		
		if(directType == false){ //если неорграф
			e = adjList.get(ivOut, ivIn);
			e = null;
			adjList.set(ivIn, ivOut, null);
		}
		
		eNum--;
		return true;
	}

	@Override
	public boolean deleteVertex(int id) {
		//TODO: проверки на удаление
		//matrix2d.remove(v.getIndex(), v.getIndex());
		
		if(id > vNum || id < 0)
			return false;

		//Если есть ребро, то не удаляем
		for (int i = 0; i < adjList.getNumRows(); i++) {
			if(i != id){
				if (adjList.get(i, id) != null) {
					deleteEdge(i, id);
					//MainFrame.GetInstace().AddConsoleMsg("Delete first the edges!" + " \n");
					//return false;
				}
			}
			else{
				for (int j = 0; j < adjList.getNumCols(i); j++) {
					if (adjList.get(i, j) != null) {
						deleteEdge(i, j);
						//MainFrame.GetInstace().AddConsoleMsg("Delete first the edges!" + " \n");
						//return false;
					}
				}
			}
		}
		
		adjList.delRow(id);
		adjList.delCol(id);
		
		reIndexVertex();
		// TODO: CREATE REINDEX IN SIMPLEGRAPH!
		vNum--;
		return true;
	}
	
	public Edge GetEdgeByVert(int vout, int vin){
		return adjList.get(vout, vin);
	}
	
	@Override
	public void printGraph(){
		MainFrame.GetInstace().AddConsoleMsg("\n");
		for(int i = 0; i < adjList.getNumRows(); i++){
			MainFrame.GetInstace().AddConsoleMsg(i + ":: ");
			for(int j = 0; j < adjList.getNumCols(i); j++){
				if(adjList.get(i, j) != null)
					MainFrame.GetInstace().AddConsoleMsg("  " + adjList.get(i, j).getWeight() + "  ");
				else
					MainFrame.GetInstace().AddConsoleMsg(" -1 ");
			}
			MainFrame.GetInstace().AddConsoleMsg("\n");
		}
		MainFrame.GetInstace().AddConsoleMsg("\n");
	}

	@Override
	public Edge getFirstEdge() {
		
		Edge e = null;
		for(int i = 0; i < vNum; i++){
			for(int j = 0; j < vNum; j++)
			e = adjList.get(i, j);
			if(e != null)
				return e;
		}
		return null;
	}
	
	private void reIndexVertex(){
		for(int i = 0; i < vertexList.size(); i++){
			vertexList.get(i).setIndex(i);
		}
	}
	
	
}
