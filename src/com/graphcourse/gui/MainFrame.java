package com.graphcourse.gui;

import java.awt.EventQueue;

import javax.swing.JFrame;

import com.graphcourse.auxiliar.GraphGenerator;
import com.graphcourse.graph.Graph;
import com.graphcourse.graph.GraphIterator;
import com.graphcourse.graph.Vertex;

import java.awt.Panel;
import java.awt.BorderLayout;
import java.awt.Color;
import java.util.Random;
import java.awt.TextArea;
import javax.swing.JButton;
import javax.swing.JPanel;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JTabbedPane;
import javax.swing.JTextField;
import javax.swing.JLabel;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;

public class MainFrame {
	
	private static MainFrame instance = new MainFrame();
	
	private Random rand = new Random();
	
	private JFrame frame;
	private TextArea textArea1;
	private JTextField textField_vNumGG;
	private JTextField textField_eNumGG;
	private JComboBox comboBox_GGdirT;
	private JComboBox comboBox_GGconT;
	private JTextField textField_delEvout;
	private JTextField textField_delEvin;
	private JTextField textField_delVid;
	private JTextField textField_insEvOut;
	private JTextField textField_insEvIn;
	
	private Graph graph;
	private Graph.GraphIterator It;
	
	public static void main(String[] args) {
		
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					new MainFrame().GetInstace().frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
		MainFrame.GetInstace().FitstInit();
		
	}

    public static MainFrame GetInstace(){
        return instance;
    }
	
    public boolean GenerateGraph(){
    	if(textField_vNumGG.getText().isEmpty() || textField_eNumGG.getText().isEmpty()){
    		AddConsoleMsg("Some textfield is empty! (Graph generator)\n");
    		return false;
    	}
    	
    	graph = null;
    	
    	int vNum = Integer.parseInt(textField_vNumGG.getText());
    	int eNum = Integer.parseInt(textField_eNumGG.getText());
    	
    	boolean dirT = false;
    	boolean conT = false;
    	
    	switch(comboBox_GGdirT.getSelectedIndex()){
	    	case 0: {
	    		System.out.println("ORGRA");
	    		dirT = true;
	    		break;
	    	}
	    	case 1:{
	    		System.out.println("NEORGRA");
	    		dirT = false;
	    		break;
	    	}
    	}
    	
    	switch(comboBox_GGconT.getSelectedIndex()){
	    	case 0:{ //MGraph
	    		conT = true;
	    		break;
	    	}
	    	case 1:{ //LGraph
	    		conT = false;
	    		break;
	    	}
    	}
    	
		GraphGenerator<Integer, Integer, Integer, Integer> gg = new GraphGenerator<>();
		gg.GenerateVertexArr(vNum);
		graph = gg.GenerateGraph(vNum, eNum, dirT, conT);
		if(graph == null)
			return false;
		graph.printGraph();
		return true;
    }
    
    public boolean DelEdge(){
    	if(!(graph != null &&  graph.getVNum() > 0 && graph.getENum() > 0)){
    		AddConsoleMsg("Cant create delEdge - some trouble\n");
    		return false;
    	}
    	
    	if(textField_delEvout.getText().isEmpty() || textField_delEvin.getText().isEmpty()){
    		AddConsoleMsg("Some textfield is empty! (DelEdge)\n");
    		return false;
    	}
    	
    	int vOut = Integer.parseInt(textField_delEvout.getText());
    	int vIn = Integer.parseInt(textField_delEvin.getText());
    	
    	if(graph.deleteEdge(vOut, vIn)){
    		AddConsoleMsg("true (DelEdge)\n");
    		return true;
    	}
    	AddConsoleMsg("false (DelEdge)\n");
    	return false;
    	
    }
    
    public boolean DelVert(){
    	if(!(graph != null &&  graph.getVNum() > 0)){
    		AddConsoleMsg("Cant create delVert - some trouble\n");
    		return false;
    	}
    	
    	if(textField_delVid.getText().isEmpty()){
    		AddConsoleMsg("Some textfield is empty! (DelVert)\n");
    		return false;
    	}
    	
    	int vId = Integer.parseInt(textField_delVid.getText());
    	
    	if(graph.deleteVertex(vId)){
    		AddConsoleMsg("true (DelVert)\n");
    		return true;
    	}
    	AddConsoleMsg("false (DelVert)\n");
    	return false;
    }
    
    public boolean InsertEdge(){
    	
    	if(!(graph != null &&  graph.getVNum() >= 0)){
    		AddConsoleMsg("Cant create insEdge - some trouble\n");
    		return false;
    	}
    	
    	if(textField_insEvOut.getText().isEmpty() || textField_insEvIn.getText().isEmpty()){
    		AddConsoleMsg("Some textfield is empty! (InsEdge)\n");
    		return false;
    	}
    	
    	int ivOut = Integer.parseInt(textField_insEvOut.getText());
    	int ivIn = Integer.parseInt(textField_insEvIn.getText());
    	
    	if(graph.insertEdge(ivOut, ivIn, 15, 15)){
    		AddConsoleMsg("true (insEdge)\n");
    		return true;
    	}
    	AddConsoleMsg("false (insEdge)\n");
    	return false;
    }
    
    public boolean InsertVert(){
    	
    	if(graph == null){
    		AddConsoleMsg("Cant create insVert - some trouble\n");
    		return false;
    	}
    	
    	if(graph.insertVertex(rand.nextInt(50), rand.nextInt(50))){
    		AddConsoleMsg("true (insVert)\n");
    		return true;
    	}
    	AddConsoleMsg("false (insVert)\n");
    	return false;
    }
    
    public void PrintGraph(){
    	if(graph != null &&  graph.getVNum() > 0)
    		graph.printGraph();
    }
    
    public boolean CreateVertIter(){
    	if(graph == null){
    		AddConsoleMsg("Cant create insVert - some trouble\n");
    		return false;
    	}
    	
    	It = graph.vertexIterator();
    	AddConsoleMsg("true (vertIter)\n");
    	return true;
    }
    
    public boolean CreateEdgeIter(){
    	if(graph == null){
    		AddConsoleMsg("Cant create edgeVert - some trouble\n");
    		return false;
    	}
    	
    	It = graph.vertexIterator();
    	AddConsoleMsg("true (edgeIter)\n");
    	return true;
    }
    
    public boolean IterBegin(){
    	if(graph == null){
    		AddConsoleMsg("Cant create itBegin - some trouble\n");
    		return false;
    	}
    	
    	if(It.begin()){
    		AddConsoleMsg("true (itBegin)\n");
    		return true;
    	}
    	AddConsoleMsg("false (itBegin)\n");
    	return false;
	}
    
    public boolean IterEnd(){
    	if(graph == null){
    		AddConsoleMsg("Cant create itBegin - some trouble\n");
    		return false;
    	}
    	
    	if(It.end()){
    		AddConsoleMsg("true (itEnd)\n");
    		return true;
    	}
    	
    	return false;
	}
    
    public boolean IterNext(){
    	if(graph == null){
    		AddConsoleMsg("Cant create itBegin - some trouble\n");
    		return false;
    	}
    	
    	if(It.next()){
    		AddConsoleMsg("true (itNext)\n");
    		return true;
    	}
    	
    	return false;
    }
    
    public boolean IterGet(){
    	if(graph == null){
    		AddConsoleMsg("Cant create itBegin - some trouble\n");
    		return false;
    	}
    	
    	Vertex v = It.get();
    	if(v != null){
    		AddConsoleMsg(v.getIndex() + "\n");
    		return true;
    	}
    	
    	return false;
    }
    
    
	public void FitstInit(){
		/*
		ArrayList<Vertex> vList = new ArrayList<Vertex>();
		ArrayList<Edge> eList = new ArrayList<Edge>();
		
		MGraph testM = new MGraph();
		
		for(int i = 0; i < 5; i++){
			Vertex v = new Vertex<Integer, Integer>(rand.nextInt(100), rand.nextInt(100), i);
			testM.insertVertex(v);
			vList.add(v);
		}
		
		for(int i = 0; i < 4; i++){
			Edge e = new Edge<Integer, Integer>(vList.get(i), vList.get(i+1), i + 5);
			testM.insertEdge(e);
			eList.add(e);
		}
		testM.PrintMatrix();
		testM.deleteVertex(vList.get(2));
		testM.PrintMatrix();
		testM.deleteEdge(eList.get(1));
		testM.PrintMatrix();
		testM.deleteVertex(vList.get(2));
		testM.PrintMatrix();
		testM.deleteEdge(eList.get(0));
		testM.PrintMatrix();
		testM.deleteVertex(vList.get(0));
		testM.PrintMatrix();
		testM.deleteVertex(vList.get(0));
		testM.PrintMatrix();
		
		LGraph testL = new LGraph();
		
		for(int i = 0; i < 10; i++){
			Vertex <Integer, Integer> v = new Vertex<Integer, Integer>(i, null, testL.getvNum());
			testL.insertVertex(v);
			vList.add(v);
		}
		for(int i = 0; i < 5; i++){
			testL.insertEdge(new Edge<Integer, Integer>(vList.get(i), vList.get(i+1), i + 5));
		}
		
		vList.add(new Vertex<Integer, Integer>(10, null, 10));
		testL.insertEdge(new Edge<Integer, Integer>(vList.get(9), vList.get(6), 5));
		
		testL.PrintAdjList();
		testL.deleteVertex(vList.get(6));
		testL.PrintAdjList();
		testL.deleteEdge(new Edge<Integer, Integer>(vList.get(9), vList.get(6), 5));
		testL.PrintAdjList();
		testL.deleteVertex(vList.get(6));
		testL.PrintAdjList();
		*/
	}
	
	public void AddConsoleMsg(String msg){
		textArea1.append(msg);
	}
	
	public void ClearTextArea(){
		textArea1.setText("");
	}
	
	public void GetGraphInfo(){
		if(graph == null){
			AddConsoleMsg("Graph not create\n");
			return;
		}
		AddConsoleMsg("\nVert.Num = " + graph.getVNum() + ";\nEdg.Num = " + graph.getENum() + ";\nK = " + graph.getK() + ";\ndirectType = ");
		if(graph.getdirectType())
			AddConsoleMsg("Orgraph;\nContainerT = ");
		else
			AddConsoleMsg("Neorgraph;\nContainerT =");
		
		if(graph.getgraphType())
			AddConsoleMsg("MGraph\n;");
		else
			AddConsoleMsg("LGraph\n;");
	}
	
	/**
	 * Create the application.
	 */
	private MainFrame() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame = new JFrame();
		frame.setBounds(100, 100, 576, 576);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setTitle("GraphCourse");
		
		Panel panel = new Panel();
		panel.setBackground(Color.WHITE);
		frame.getContentPane().add(panel, BorderLayout.CENTER);
		panel.setLayout(null);
		
		textArea1 = new TextArea();
		textArea1.setEditable(false);
		textArea1.setBounds(10, 10, 540, 174);
		panel.add(textArea1);
		
		JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.TOP);
		tabbedPane.setBounds(10, 214, 540, 313);
		panel.add(tabbedPane);
		
		JPanel panel_1 = new JPanel();
		tabbedPane.addTab("Graph UI", null, panel_1, null);
		
		JButton btnGGnew = new JButton("Generate");
		btnGGnew.setBounds(10, 96, 82, 23);
		btnGGnew.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				GenerateGraph();
			}
		});
		panel_1.setLayout(null);
		panel_1.add(btnGGnew);
		
		textField_vNumGG = new JTextField();
		textField_vNumGG.setBounds(71, 12, 58, 20);
		panel_1.add(textField_vNumGG);
		textField_vNumGG.setColumns(10);
		
		JLabel lblVertexnum = new JLabel("VertexNum:");
		lblVertexnum.setBounds(10, 14, 58, 17);
		panel_1.add(lblVertexnum);
		
		JLabel lblEdgenum = new JLabel("EdgeNum:");
		lblEdgenum.setBounds(10, 37, 58, 16);
		panel_1.add(lblEdgenum);
		
		textField_eNumGG = new JTextField();
		textField_eNumGG.setBounds(71, 34, 58, 20);
		panel_1.add(textField_eNumGG);
		textField_eNumGG.setColumns(10);
		
		comboBox_GGdirT = new JComboBox();
		comboBox_GGdirT.setModel(new DefaultComboBoxModel(new String[] {"Orgraph", "Neorgraph"}));
		comboBox_GGdirT.setToolTipText("");
		comboBox_GGdirT.setBounds(10, 64, 76, 20);
		panel_1.add(comboBox_GGdirT);
		
		comboBox_GGconT = new JComboBox();
		comboBox_GGconT.setModel(new DefaultComboBoxModel(new String[] {"MGraph", "LGraph"}));
		comboBox_GGconT.setBounds(96, 64, 76, 20);
		panel_1.add(comboBox_GGconT);
		
		JLabel lblDeledge = new JLabel("DelEdge:");
		lblDeledge.setBounds(5, 130, 60, 14);
		panel_1.add(lblDeledge);
		
		JLabel lblVout = new JLabel("vOut:");
		lblVout.setBounds(71, 130, 36, 14);
		panel_1.add(lblVout);
		
		textField_delEvout = new JTextField();
		textField_delEvout.setBounds(112, 127, 30, 20);
		panel_1.add(textField_delEvout);
		textField_delEvout.setColumns(10);
		
		JLabel lblVin = new JLabel("vIn:");
		lblVin.setBounds(71, 155, 36, 14);
		panel_1.add(lblVin);
		
		textField_delEvin = new JTextField();
		textField_delEvin.setBounds(112, 152, 30, 20);
		panel_1.add(textField_delEvin);
		textField_delEvin.setColumns(10);
		
		JButton btnDele = new JButton("DelE");
		btnDele.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				DelEdge();
			}
		});
		btnDele.setBounds(5, 151, 60, 23);
		panel_1.add(btnDele);
		
		JButton btnPrintg = new JButton("PrintG");
		btnPrintg.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				PrintGraph();
			}
		});
		btnPrintg.setBounds(440, 258, 89, 23);
		panel_1.add(btnPrintg);
		
		JLabel lblDelvert = new JLabel("DelVert:");
		lblDelvert.setBounds(152, 130, 46, 14);
		panel_1.add(lblDelvert);
		
		JLabel lblId = new JLabel("ID:");
		lblId.setBounds(198, 130, 25, 14);
		panel_1.add(lblId);
		
		textField_delVid = new JTextField();
		textField_delVid.setBounds(226, 127, 30, 20);
		panel_1.add(textField_delVid);
		textField_delVid.setColumns(10);
		
		JButton btnDelv = new JButton("DelV");
		btnDelv.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				DelVert();
			}
		});
		btnDelv.setBounds(152, 151, 60, 23);
		panel_1.add(btnDelv);
		
		JLabel lblInsedge = new JLabel("InsEdge:");
		lblInsedge.setBounds(5, 185, 55, 14);
		panel_1.add(lblInsedge);
		
		JLabel lblVout_1 = new JLabel("vOut:");
		lblVout_1.setBounds(71, 185, 36, 14);
		panel_1.add(lblVout_1);
		
		textField_insEvOut = new JTextField();
		textField_insEvOut.setBounds(112, 183, 30, 20);
		panel_1.add(textField_insEvOut);
		textField_insEvOut.setColumns(10);
		
		JLabel lblVin_1 = new JLabel("vIn:");
		lblVin_1.setBounds(71, 208, 32, 14);
		panel_1.add(lblVin_1);
		
		textField_insEvIn = new JTextField();
		textField_insEvIn.setBounds(112, 205, 30, 20);
		panel_1.add(textField_insEvIn);
		textField_insEvIn.setColumns(10);
		
		JButton btnInse = new JButton("InsE");
		btnInse.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				InsertEdge();
			}
		});
		btnInse.setBounds(5, 204, 60, 23);
		panel_1.add(btnInse);
		
		JLabel lblInsvert = new JLabel("InsRandVert:");
		lblInsvert.setBounds(152, 185, 75, 14);
		panel_1.add(lblInsvert);
		
		JButton btnInsv = new JButton("InsV");
		btnInsv.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			InsertVert();
			}
		});
		btnInsv.setBounds(152, 204, 60, 23);
		panel_1.add(btnInsv);
		
		JButton btnGraphinfo = new JButton("GraphInfo");
		btnGraphinfo.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				GetGraphInfo();
			}
		});
		btnGraphinfo.setBounds(341, 258, 89, 23);
		panel_1.add(btnGraphinfo);
		
		JLabel lblCreate = new JLabel("Create:");
		lblCreate.setBounds(258, 15, 42, 14);
		panel_1.add(lblCreate);
		
		JButton btnVertit = new JButton("VertIT");
		btnVertit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				CreateVertIter();
			}
		});
		btnVertit.setBounds(300, 11, 70, 23);
		panel_1.add(btnVertit);
		
		JButton btnNewButton = new JButton("EdgIT");
		btnNewButton.setBounds(374, 11, 70, 23);
		panel_1.add(btnNewButton);
		
		JButton btnOutedgit = new JButton("outEdgIt");
		btnOutedgit.setBounds(448, 11, 82, 23);
		panel_1.add(btnOutedgit);
		
		JButton btnBegin = new JButton("Begin");
		btnBegin.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				IterBegin();
			}
		});
		btnBegin.setBounds(276, 37, 68, 23);
		panel_1.add(btnBegin);
		
		JButton btnNewButton_2 = new JButton("End");
		btnNewButton_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				IterEnd();
			}
		});
		btnNewButton_2.setBounds(348, 37, 58, 23);
		panel_1.add(btnNewButton_2);
		
		JButton btnNext = new JButton("Next");
		btnNext.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				IterNext();
			}
		});
		btnNext.setBounds(410, 37, 60, 23);
		panel_1.add(btnNext);
		
		JButton btnGet = new JButton("Get");
		btnGet.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				IterGet();
			}
		});
		btnGet.setBounds(474, 37, 58, 23);
		panel_1.add(btnGet);
		
		JPanel panel_2 = new JPanel();
		tabbedPane.addTab("Task 1", null, panel_2, null);
		panel_2.setLayout(null);
		
		JButton btnNewButton_1 = new JButton("New button");
		btnNewButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				AddConsoleMsg("2");
			}
		});
		btnNewButton_1.setBounds(10, 38, 89, 23);
		panel_2.add(btnNewButton_1);
		
		JPanel panel_3 = new JPanel();
		tabbedPane.addTab("Task 2", null, panel_3, null);
		panel_3.setLayout(null);
		
		JButton btnClear = new JButton("Clear");
		btnClear.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				ClearTextArea();
			}
		});
		btnClear.setBounds(478, 190, 72, 23);
		panel.add(btnClear);
		
		
		
	}
}
