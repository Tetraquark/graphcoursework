package com.graphcourse.graph;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;

import com.graphcourse.gui.MainFrame;

public class LGraph <VN, VT, EW, ET> extends Graph <VN, VT, EW, ET>{
	
	private ArrayList<LinkedList<Edge>> adjList;
	
	public LGraph(){
		adjList = new ArrayList<LinkedList<Edge>>();
		this.directType = true;
		this.graphType = false;
		vNum = 0; 
		eNum = 0;
	}
	
	public LGraph(boolean directType){
		adjList = new ArrayList<LinkedList<Edge>>();
		this.directType = directType;
		this.graphType = false;
		vNum = 0; 
		eNum = 0;
	}
	
	@Override
	public boolean insertEdge(int ivOut, int ivIn, EW weight, ET data) {
		
		if(ivOut > vNum || ivOut < 0 || ivIn > vNum || ivIn < 0)
			return false;
		
		if(eNum >= getEdgeMax())
			return false;
		
		Vertex vIn = vertexList.get(ivIn);
		Vertex vOut = vertexList.get(ivOut);
		
		if(vOut == null || vIn == null)
			return false;
		
		Edge e = new Edge<EW, ET>(vOut, vIn, weight, data);
		LinkedList<Edge> list = adjList.get(e.getvOut().getIndex());
		if(list != null)
			list.add(e);
		else
			return false;
		
		if(directType == false){
			list = adjList.get(e.getvIn().getIndex());
			if(list != null){
				list.add(new Edge(e.getvIn(), e.getvOut(), e.getWeight(), e.getData() ));
			}
			else{
				adjList.get(e.getvOut().getIndex()).remove(e);
				MainFrame.GetInstace().AddConsoleMsg("Dont add edges - not find" + e.getvIn().getName() + "/n");
				return false;
			}
		}
		
		eNum++;
		return true;
	}

	@Override
	public boolean insertVertex(VN name, VT data) {
		
		Vertex <VN, VT> v = new Vertex <VN, VT>(name, data, vNum);
		
		vertexList.add(v);
		adjList.add(new LinkedList<Edge>());
		vNum++;
		return true;
	}

	@Override
	public boolean deleteEdge(int ivOut, int ivIn) {
		
		if(ivOut > vNum || ivOut < 0 || ivIn > vNum || ivIn < 0){
			return false;
		}
		
		LinkedList list = adjList.get(ivOut);
		Iterator it = list.iterator();
		Edge delE = null;
		
		for(int i = 0; i < list.size(); i++){
			delE = (Edge) list.get(i);
			if(ivIn == delE.getvIn().getIndex()){
				delE = null;
				list.remove(i);
				if(directType == true){ //Если орграф
					eNum--;
					return true;
				}
				break;
			}
		}
		
		if(directType == false){ // Если неорграф
			list = adjList.get(ivIn);
			for(int i = 0; i < list.size(); i++){
				delE = (Edge) list.get(i);
				if(ivOut == delE.getvIn().getIndex()){
					delE = null;
					list.remove(i);
					eNum--;
					return true;
				}
			}
		}
		
		MainFrame.GetInstace().AddConsoleMsg("Not delete!" + " \n");
		return false;
	}

	@Override
	public boolean deleteVertex(int id) {
		
		if(id < 0 || id > vNum)
			return false;
		
		if(id < adjList.size() && id >= 0){
			LinkedList delList = adjList.get(id);
			
			if(delList.isEmpty()){ //если у вершины нет ребер
				if(directType == true){ //если орграф
					for(int i = 0; i < adjList.size(); i++){
						if(id != i){
							LinkedList list = adjList.get(i);
							if(!list.isEmpty()){
								for(int j = 0; j < list.size(); j++){
									Edge e = (Edge) list.get(j);
									if(e.getvIn().getIndex() == id){
										e = null;
										list.remove(j);
										eNum--;
									}
								}
							}
						}
					}
				}
			}
			else{ // если у вершины есть ребра

				if(directType){ // если орграф
					for(int i = 0; i < adjList.size(); i++){
						if(id != i){
							LinkedList list = adjList.get(i);
							if(!list.isEmpty()){
								for(int j = 0; j < list.size(); j++){
									Edge e = (Edge) list.get(j);
									if(e.getvIn().getIndex() == id){
										e = null;
										list.remove(j);
										eNum--;
									}
								}
							}
						}
					}
				}
				else{ // если неорграф
					
					for(int i = 0; i < delList.size(); i++){
						Edge e = (Edge) delList.get(i);
						deleteEdge(e.getvIn().getIndex(), e.getvOut().getIndex());
					}
				}
					
				for(int i = 0; i < delList.size(); i++){
					Edge e = (Edge) delList.get(i);
					deleteEdge(e.getvOut().getIndex(), e.getvIn().getIndex());
				}

			}
			delList.clear();
		}
		else{
			MainFrame.GetInstace().AddConsoleMsg("Exception delete Vertex" + " \n");
			return false;
		}
		
		adjList.remove(id);
		
		Vertex v = vertexList.get(id);
		v = null;
		vertexList.remove(id);
		
		reIndexVertex(id);
		vNum--;
		
		MainFrame.GetInstace().AddConsoleMsg("Deleted vertex:" + id + " \n");
		return true;
	}
	
	@Override
	public void printGraph(){
		MainFrame.GetInstace().AddConsoleMsg(" \n");
		
		for(int i = 0; i < adjList.size(); i++){
			MainFrame.GetInstace().AddConsoleMsg(i + " : ");
			Iterator it = adjList.get(i).iterator();
			while(it.hasNext()){
				Edge e = (Edge) it.next();
				if(e != null)
					MainFrame.GetInstace().AddConsoleMsg(e.getvOut().getIndex() + " -> " + e.getvIn().getIndex() + " ");
			}
			MainFrame.GetInstace().AddConsoleMsg(" \n");
		}
	}
	
	@Override
	public Edge getFirstEdge(){
		if(eNum == 0){
			return null;
		}
		Edge e;

		for(int i = 0; i < vNum; i++){
			LinkedList list = adjList.get(i);
			if(!list.isEmpty()){
				for(int j = 0; j < list.size(); j++){
					e = (Edge) list.get(j);
					if(e != null)
						return e;
				}
			}
		}
		
		return null;
	}
	
	/*
	public class EdgeIter implements GraphIterator{
		private Graph graph;
		private Edge edge;
		
		public EdgeIter(Graph graph){
			this.graph = graph;
		}
		
		public EdgeIter(Graph graph, Edge e){
			this.graph = graph;
			this.edge = e;
		}
		
		@Override
		public GraphIterator beg() {
			edge = graph.getFirstEdge();
			if(!end())
				return new EdgeIter(graph, edge);
			return null;
		}

		@Override
		public boolean end() {
			return edge == null;
		}

		@Override
		public boolean next() {
			if(end())
				return false;
			
			return true;
			
			
		}

		@Override
		public Object get() {
			return edge;
		}

	}
	*/
	
	private void reIndexVertex(int id){
		for(int i = id; i < vertexList.size(); i++){
			vertexList.get(i).setIndex(i);
		}
	}
}
