package com.graphcourse.graph;

/**
 * @param <N> - вес ребра
 * @param <T> - данные ребра
 */

public class Edge <W, T> {
	
	private Vertex vOut, vIn; //vOut - выходит, vIn - входит
	private W weight;
	private T data;
	
	public Edge(){
		this.vOut = null;
		this.vIn = null;
		this.weight = null;
		this.data = null;
	}
	
	public Edge(Vertex vOut, Vertex vIn){
		this.vOut = vOut;
		this.vIn = vIn;
		this.weight = null;
		this.data = null;
	}
	
	public Edge(Vertex vOut, Vertex vIn, W weight){
		this.vOut = vOut;
		this.vIn = vIn;
		this.weight = weight;
		this.data = null;
	}
	
	public Edge(Vertex vOut, Vertex vIn, W weight, T data){
		this.vOut = vOut;
		this.vIn = vIn;
		this.weight = weight;
		this.data = data;
	}
	
	public boolean Compare(Edge e){
		if(this.vOut == e.getvOut() && this.vIn == e.getvIn())
			return true;
		return false;
	}

	public Vertex getvOut() {
		return vOut;
	}

	public void setvOut(Vertex vOut) {
		this.vOut = vOut;
	}

	public Vertex getvIn() {
		return vIn;
	}

	public void setvIn(Vertex vIn) {
		this.vIn = vIn;
	}

	public W getWeight() {
		return weight;
	}

	public void setWeight(W weight) {
		this.weight = weight;
	}

	public T getData() {
		return data;
	}

	public void setData(T data) {
		this.data = data;
	}
	
	
}
