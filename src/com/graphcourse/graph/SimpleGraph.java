package com.graphcourse.graph;

import java.util.ArrayList;

import com.graphcourse.auxiliar.GraphGenerator;

/**
 * @param <VN> тип имени вершины
 * @param <VT> тип данных вершины
 * @param <EN> тип веса ребра
 * @param <ET> тип данных ребра
 */

public class SimpleGraph <VN, VT, EN, ET> {
	
	private boolean directType; //true = орграф, false = неорграф
	private boolean graphType; // L-граф = false; M-граф = true
	
	private int vNum; 
	private int eNum; 
	private int k = 0;
	
	private ArrayList <Vertex<VN, VT>> vertexList;
	
	private Graph graphContainer;
	
	public SimpleGraph(){
		this.directType = false;
		this.graphType = false;
		
		this.graphContainer = new LGraph(directType);
		this.vNum = 0;
		this.eNum = 0;
		this.k = 0;
	}
	
	public SimpleGraph(int vNum, boolean directType, boolean graphType){
		this.eNum = 0;
		this.vNum = vNum;
		this.directType = directType;
		this.graphType = graphType;
		
		GraphGenerator gg = new GraphGenerator();
		
		this.vertexList = gg.GetVertexArr();
		this.graphContainer = gg.GenerateGraph(vNum, 0, directType, graphType);
		this.k = mathK();
	}
	
	public SimpleGraph(int vNum, int eNum, boolean directType, boolean graphType){
		this.vNum = vNum;
		this.eNum = eNum;
		
		if(directType){
			if(eNum > vNum * (vNum - 1)){
				eNum = vNum * (vNum - 1);
			}
		}
		else{
			if(eNum > vNum * (vNum - 1) / 2){
				eNum = vNum * (vNum - 1) / 2;
			}
		}
		
		this.directType = directType;
		this.graphType = graphType;
		
		GraphGenerator gg = new GraphGenerator();
		
		this.vertexList = gg.GetVertexArr();
		this.graphContainer = gg.GenerateGraph(vNum, eNum, directType, graphType);
		
	}
	
	/**
	 * Конструктор копирования
	 */
	public SimpleGraph(Graph g){
		
	}
	
	public int GetVertexNum(){
		return vNum;
	}
	
	public int GetEdgeNum(){
		return eNum;
	}
	
	public boolean GetGraphDirect(){
		return directType;
	}
	
	public boolean GetGraphType(){
		return graphType;
	}
	
	public int GetK(){
		return mathK();
	}
	
	public boolean ToListGraph(){
		if(graphType){ //если мграф
			Graph mGr = new MGraph(directType);

		}
		return false;
	}
	
	public void ToMatrixGraph(){
		// преобразование к М графу
	}
	
	public Vertex InsertVertex(){
		
		return null;
	}
	
	public boolean DeleteVertex(Vertex v){
		return false;
	}
	
	public Edge InsertEdge(Vertex vOut, Vertex vIn){
		return null;
	}
	
	public boolean DeleteEdge(Vertex vOut, Vertex vIn){
		return false;
	}
	
	public Edge GetEdge(Vertex vOut, Vertex vIn){
		return null;
	}
	
	public Vertex GetVertexByIndex(int index){
		for(int i = 0; i < vertexList.size(); i++){
			if(vertexList.get(i).getIndex() == index)
				return vertexList.get(i);
		}
		return null;
	}
	
	public void printGraph(){
		graphContainer.printGraph();
	}
	
	private int mathK(){
		int kn = 0;
		if(directType) //true - orgraph
			kn = eNum / (vNum * (vNum - 1));
		else // false - neorgr
			kn = eNum / ((vNum * (vNum - 1)) / 2);
		return kn;
	}
	
	private void reIndexVertex(){
		for(int i = 0; i < vertexList.size(); i++){
			vertexList.get(i).setIndex(i);
		}
	}
	
	/*
	public class VertexIterator implements GraphIterator{
		
		private SimpleGraph sGraph;
		private Vertex vertex;
		
		public VertexIterator(SimpleGraph g) {
			this.sGraph = g;
			this.vertex = null;
		}
		
		public VertexIterator(SimpleGraph g, Vertex v) {
			this.sGraph = g;
			this.vertex = v;
		}
		
		@Override
		public GraphIterator beg() {
			if(sGraph.GetVertexNum() != 0)
				return new VertexIterator(sGraph, vertex);
			return null;
		}
		
		// is iterator in list end
		@Override
		public boolean end() {
			if(vertex != null)
			if(vertex.getIndex() == sGraph.GetVertexNum())
				return true;
			return false;
		}

		@Override
		public boolean next() {
			return false;
		}

		@Override
		public Object get() {
			if(vertex != null)
				return vertex;
			return null;
		}
		
	}
	*/
}
