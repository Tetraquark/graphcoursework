package com.graphcourse.graph;

import java.util.ArrayList;

public abstract class Graph <VN, VT, EW, ET>{
	
	protected boolean directType;	// неорграф = false; орграф = true
	protected boolean graphType; // L-граф = false; M-граф = true
	
	protected int eNum;			// кол.во ребер
	protected int vNum;			// кол.во вершины
	protected double k;			// коэф.насыщения
	
	protected ArrayList <Vertex<VN, VT>> vertexList;
	
	public Graph(){
		vertexList = new ArrayList<Vertex<VN, VT>>();
	}
	
	public Graph(Graph g){
		this.vNum = 0;
		this.eNum = 0;
		this.k = 0;
	}
	
	public boolean getdirectType() {
		return directType;
	}
	
	public boolean getgraphType(){
		return graphType;
	}
	
	public void setdirectType(boolean directType) {
		this.directType = directType;
	}

	public int getENum() {
		return eNum;
	}
	
	public void setVNum(int eNum) {
		this.eNum = eNum;
	}
	
	public int getVNum() {
		return vNum;
	}
	
	public void setENum(int vNum) {
		this.vNum = vNum;
	}
	
	public boolean ToListGraph(){
		return false;
	}
	
	public boolean ToMatrixGraph(){
		return false;
	}
	
	public double getK(){
		if(eNum == 0 || vNum == 0)
			return 0;
		
		if(directType)
			k = eNum / getEdgeMax();
		else
			k = eNum / getEdgeMax(); 
		return k;
	}
	
	public int getEdgeMax(){
		if(directType)
			return vNum * (vNum - 1);
		else
			return vNum * (vNum - 1) / 2;
	}
	
	public GraphIterator vertexIterator(){
		return new VertexIterator(this);
	}
	
	public abstract boolean insertEdge(int ivOut, int ivIn, EW weight, ET data);
	public abstract boolean insertVertex(VN name, VT data);
	public abstract boolean deleteEdge(int ivOut, int ivIn);
	public abstract boolean deleteVertex(int id);
	public abstract void printGraph();
	
	public abstract Edge getFirstEdge();
	
	public abstract class GraphIterator {
		public abstract boolean begin();
		public abstract boolean end();
		public abstract boolean next();
		public abstract Vertex get();
	}
	
	
	public class VertexIterator extends GraphIterator{

		private Graph graph;
		private Vertex vertex;
		
		public VertexIterator(Graph g){
			this.graph = g;
			this.vertex = null;
		}
		
		@Override
		public boolean begin() {
			if(graph == null)
				return false;
			if(vertexList.size() <= 0){
				System.out.println(vertexList.size());
				return false;
			}
			vertex = vertexList.get(0);
			return true;
		}

		@Override
		public boolean end() {
			if(!isValid())
				return false;
			vertex = vertexList.get(vertexList.size() - 1);
			return true;
		}

		@Override
		public boolean next() {
			if(isValid()){
				vertex = vertexList.get(vertex.getIndex() + 1);

				return true;
			}
			return false;
		}

		@Override
		public Vertex get() {
			return vertex;
		}
		
		public boolean isValid(){
			if(vertex == null || graph== null)
				return false;
			if(vertex.getIndex() >= vertexList.size())
				return false;
			
			return true;
		}
	}
}
